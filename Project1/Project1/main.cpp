#include <algorithm>
#include <string>
#include <vector>
#include <windows.h>

#include "dirrent.h"

#include "prints.h"
#include "types.h"

std::vector<module *> modules;
std::vector<module *> selectedModules;

char *toUpper(char *pArray) {
  int arrayLength = strlen(pArray);

  for (int i = 0; i < arrayLength; i++) {
    if (pArray[i] >= 'a' && pArray[i] <= 'z')
      pArray[i] -= ' ';
  }

  return pArray;
}

std::vector<std::string> *listdir(const char *dirname) {
  DIR *dp;
  dirent *d;
  std::vector<std::string> *vec = new std::vector<std::string>;

  dp = opendir(dirname);
  while ((d = readdir(dp)) != NULL) {
    vec->push_back(d->d_name);
  }

  sort(vec->begin(), vec->end());
  return vec;
}

void reloadModules() {
  modules.clear();
  std::vector<std::string> *strModules = listdir("modules/");


  for (unsigned int i = 0; i < strModules->size(); i++) {

    if (strcmp(strModules->at(i).c_str(), ".") == 0 ||
        strcmp(strModules->at(i).c_str(), "..") == 0) {

    } else {
      module *m = new module();
      memcpy(m->path, ("modules\\" + strModules->at(i)).c_str(),
             strlen(("modules\\" + strModules->at(i)).c_str()));
      memcpy(m->name, strModules->at(i).c_str(),
             strlen(strModules->at(i).c_str()));

      // printf("%s\n", strModules->at(i).c_str());
      modules.push_back(m);
    }
  }
}

void addModule(char *name) {
  bool found = false;

  for (unsigned int i = 0; i < modules.size(); i++) {
    if (strcmp(name, modules[i]->name) == 0) {
      found = true;
      selectedModules.push_back(modules[i]);
    }
  }

  if (found == false) {
    printError("module `%s` does not exists", name);
  }
}

void showOptions(char *name) {
  if (name == nullptr) {
    for (unsigned int i = 0; i < selectedModules.size(); i++) {
      char cmd[256];
      char buf[256];
      GetCurrentDirectoryA(256, buf);
      sprintf(cmd, "cd \"%s\\\" & \"%s.exe\" options && cd \"%s\"",
              selectedModules[i]->path, selectedModules[i]->name, buf);

      printf("\n");
      system(cmd);
      printf("\n");
    }
  }
}

void setOption(char *arg0, char *arg1, char *arg2) {
  module *foundModule = nullptr;

  char cmd[256];
  char buf[256];

  // search for module by arg0
  for (unsigned int i = 0; i < selectedModules.size(); i++) {
    if (strcmp(arg0, selectedModules[i]->name) == 0) {
      foundModule = selectedModules[i];
      break;
    }
  }

  if (foundModule) {
    printf("found modules\n");
    GetCurrentDirectoryA(256, buf);
    sprintf(cmd, "cd \"%s\\\" & \"%s.exe\" set %s %s && cd \"%s\"",
            foundModule->path, foundModule->name, arg1, arg2, buf);

    printf("\n");
    system(cmd);
    printf("\n");
  } else {
    if (!arg2) {
      if (selectedModules.size() == 1) {
        GetCurrentDirectoryA(256, buf);
        sprintf(cmd, "cd \"%s\\\" & \"%s.exe\" set %s %s && cd \"%s\"",
                selectedModules[0]->path, selectedModules[0]->name, arg0, arg1,
                buf);

        printf("\n");
        system(cmd);
        printf("\n");
      } else {
        for (;;) {
          printf("set `%s` = %s for all selected modules ? [y/n] > ",
                 toUpper(arg0), arg1);
          fgets(buf, sizeof(buf), stdin);

          if (_stricmp(buf, "y\n") == 0) {
            for (unsigned int i = 0; i < selectedModules.size(); i++) {
              GetCurrentDirectoryA(256, buf);
              sprintf(cmd, "cd \"%s\\\" & \"%s.exe\" set %s %s && cd \"%s\"",
                      selectedModules[i]->path, selectedModules[i]->name, arg0,
                      arg1, buf);

              printf("\n");
              system(cmd);
              printf("\n");

              return;
            }
          } else if (_stricmp(buf, "n\n") == 0) {
            return;
          }
        }
      }
    } else {
      printError("didnt find module %s", arg0);
    }
  }
}

void run() {
  for (unsigned int i = 0; i < selectedModules.size(); i++) {
    char cmd[256];
    char buf[256];
    GetCurrentDirectoryA(256, buf);
    sprintf(cmd, "cd \"%s\\\" & \"%s.exe\" run && cd \"%s\"",
            selectedModules[i]->path, selectedModules[i]->name, buf);

    printf("\n");
    system(cmd);
    printf("\n");
  }
}

void parseTerminal(char *line) {
  // rm new line at the end
  line[strlen(line) - 1] = 0x00;

  // some short cuts
  if (_stricmp(line, "lo") == 0 || _stricmp(line, "ls") == 0) {
    memcpy(line, "show options", strlen("show options") + 1);
  } else if(_stricmp(line, "lm") == 0) { 
    memcpy(line, "show modules", strlen("show modules") + 1);
  }

  char *arg = strtok(line, " ");

  if (_stricmp(arg, "exit") == 0) {
    ExitProcess(0);
  } else if (strcmp(arg, "list") == 0 || strcmp(arg, "show") == 0) {
    arg = strtok(nullptr, " ");

    if (_stricmp(arg, "modules") == 0) {
      reloadModules();
      printModules(modules);
    } else if (_stricmp(arg, "options") == 0) {
      showOptions(nullptr);
    } else {
      printError("unknown `%s` in list/show", arg);
    }
  } else if (_stricmp(arg, "use") == 0) {
    arg = strtok(nullptr, " ");

    reloadModules();
    addModule(arg);
  } else if (_stricmp(arg, "set") == 0) {
    char *arg0 = strtok(nullptr, " ");
    char *arg1 = strtok(nullptr, " ");
    char *arg2 = strtok(nullptr, " ");
    setOption(arg0, arg1, arg2);
  } else if (_stricmp(arg, "run") == 0) {
    run();
  } else if (_stricmp(arg, "clear") == 0) {
    selectedModules.clear();
  } else {
    printError("unknown `%s`", arg);
  }
}

int main() {
  char buf[256];

  for (;;) {
    printTerminal(selectedModules);
    fgets(buf, sizeof(buf), stdin);
    parseTerminal(buf);
  }

  return 0;
}