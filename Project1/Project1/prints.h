#ifndef PRINTS
#define PRINTS

#include <stdio.h>
#include <stdarg.h>
#include <vector>

#include "consts.h"
#include "types.h"


void printTerminal(std::vector<module *> selectedModules) {
  printf("%s: ", projectName);

  for (unsigned int i = 0; i < selectedModules.size(); i++) {
    printf("[%s]", selectedModules[i]->name);
  }

  printf(" > ");
}

void printModules(std::vector<module *> modules) {
  printf("\n");

  for (unsigned int i = 0; i < modules.size(); i++) {
    printf("%s\n", modules[i]->name);
  }

  printf("\n");
}

void printError(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  printf("\n%s: [ERROR]: ", projectName);
  vfprintf(stdout, fmt, args);
  printf("\n\n");

  va_end(args);
}

#endif
