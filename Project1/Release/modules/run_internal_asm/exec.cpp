#include <stdio.h>
#include <windows.h>
_declspec(naked) void asmfunc() {
__asm {
mov eax, 0x0003DFD2
push eax
push 0x24
mov ecx, 0x00AC1E40
mov eax, 0x0043B950
call eax
ret
}
}
_declspec(naked) void endfunc() {}
int main() {
void *target = OpenProcess(
PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ |
PROCESS_VM_WRITE | PROCESS_VM_OPERATION,
FALSE, 4032);
void *alloc = 
VirtualAllocEx(target, NULL, 
(unsigned long)&endfunc - (unsigned long)&asmfunc,
MEM_RESERVE | MEM_COMMIT,
PAGE_EXECUTE_READWRITE);
WriteProcessMemory(target, alloc, &asmfunc,
(unsigned long)&endfunc - (unsigned long)&asmfunc,
NULL);
void *thread = CreateRemoteThread(
target, NULL, 0, (LPTHREAD_START_ROUTINE)alloc, alloc, NULL, NULL);
if (thread) {
printf("success %08X\n", (unsigned int)alloc);
} else {
printf("failed\n");
}}
