#include "consts.h"
#include "prints.h"

#include <ctype.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <typeinfo>
#include <vector>
#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>

typedef VOID(NTAPI *pTerminateProcess)();
typedef VOID(NTAPI *pZwTerminateProcess)();
typedef VOID(NTAPI *pExitProcess)();
typedef VOID(NTAPI *pRtlExitUserProcess)();
typedef VOID(NTAPI *pexit)();

pTerminateProcess RETerminateProcess;
pZwTerminateProcess ZwTerminateProcess;
pExitProcess REExitProcess;
pRtlExitUserProcess RtlExitUserProcess;
pexit REexit;

struct call {
  void *cal;
  char name[256];

  call(void *call, const char *name) {
    memcpy(this->name, name, strlen(name) + 1);
    this->cal = call;
  }
};

std::vector<call *> calls;

char TARGET[256];
char PID[256];

char *toUpper(char *pArray) {
  int arrayLength = strlen(pArray);

  for (int i = 0; i < arrayLength; i++) {
    if (pArray[i] >= 'a' && pArray[i] <= 'z')
      pArray[i] -= ' ';
  }

  return pArray;
}

bool isNumberArray(char *string) {
  for (int i = 0; i < strlen(string); i++) {
    if (!isdigit(string[i])) {
      return false;
    }
  }

  return true;
}

void loadOptions() {
  std::string line;
  std::ifstream infile("options");

  while (std::getline(infile, line)) {
    char *arg = strtok((char *)line.c_str(), ":");

    if (_stricmp(arg, "TARGET") == 0) {
      memcpy(TARGET, line.c_str() + 7, strlen(line.c_str() + 7) + 1);
    } else if (_stricmp(arg, "PID") == 0) {
      memcpy(PID, line.c_str() + 4, strlen(line.c_str() + 4) + 1);
    }
  }
}

void setOption(int i, char *argv[]) {
  if (_stricmp(argv[i + 1], "TARGET") == 0) {
    PID[0] = 0x0;
    memcpy(TARGET, argv[i + 2], strlen(argv[i + 2]) + 1);
  } else if (_stricmp(argv[i + 1], "PID") == 0) {
    if (isNumberArray(argv[i + 2])) {
      TARGET[0] = 0x0;
      memcpy(PID, argv[i + 2], strlen(argv[i + 2]) + 1);
    }
  }

  std::ofstream file;
  file.open("options");
  file << "TARGET:" << TARGET << "\n";
  file << "PID:" << PID << "\n";
  file.close();

  printf("%s: set `%s` = %s\n", projectName, toUpper(argv[i + 1]), argv[i + 2]);
}

void showOptions() {
  printf("Option                 Value  Description\n");
  printf("------                 -----  -----------\n");
  printf("TARGET  %20s  process name to target only one can be selected\n",
         TARGET);
  printf("PID     %20s  process id to target\n", PID);
}

unsigned long getProc(char *name) {
  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32)) {
    if (_stricmp(pe32.szExeFile, name) == 0) {
      return pe32.th32ProcessID;
    }
  }
  return 0;
}

unsigned long get_module(void *handle, const char *name) {
  HMODULE hMods[1024];
  DWORD cbNeeded;
  unsigned int i;

  if (EnumProcessModules(handle, hMods, sizeof(hMods), &cbNeeded)) {
    for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++) {
      TCHAR szModName[MAX_PATH];

      if (GetModuleFileNameEx(handle, hMods[i], szModName,
                              sizeof(szModName) / sizeof(TCHAR))) {

        unsigned int j;
        for (j = strlen(szModName) - 1; j > 0; j--) {
          if (szModName[j] == '\\') {
            j++;
            break;
          }
        }

        if (_stricmp(szModName + j, name) == 0) {
          return (unsigned long)hMods[i];
        }
      }
    }
  }

  return 0;
}

void initialize(void *handle) {
  HMODULE kernelbase = GetModuleHandleA("KERNELBASE.dll");
  HMODULE kernel32 = GetModuleHandleA("KERNEL32.dll");
  HMODULE ntdll = GetModuleHandleA("ntdll.dll");
  HMODULE msvcr120 = LoadLibraryA("msvcr120.dll");
  unsigned long ul_msvcr120 = get_module(handle, "msvcr120.dll");

  RETerminateProcess =
      (pTerminateProcess)GetProcAddress(kernelbase, "TerminateProcess");
  ZwTerminateProcess =
      (pZwTerminateProcess)GetProcAddress(ntdll, "ZwTerminateProcess");
  REExitProcess = (pExitProcess)GetProcAddress(ntdll, "ExitProcess");
  RtlExitUserProcess =
      (pRtlExitUserProcess)GetProcAddress(ntdll, "RtlExitUserProcess");
  REexit =
      (pexit)(ul_msvcr120 + ((unsigned long)GetProcAddress(msvcr120, "exit") -
                             (unsigned long)msvcr120));

  calls.push_back(new call(TerminateProcess, "KERNELBASE.TerminateProcess"));
  calls.push_back(new call(ZwTerminateProcess, "ntdll.ZwTerminateProcess"));
  calls.push_back(new call(ExitProcess, "KERNEL32.ExitProcess"));
  calls.push_back(new call(RtlExitUserProcess, "ntdll.RtlExitUserProcess"));
  calls.push_back(new call(REexit, "msvcr120.exit"));
}

void run() {
  void *handle;
  unsigned long pid;
  unsigned long oldProtect;
  unsigned long esp;

  esp = 0;

  if (TARGET[0]) {
    printf("using `TARGET`: %s to find proccess\n", TARGET);
    pid = getProc(TARGET);
  } else if (PID[0]) {
    pid = atoi(PID);
  } else {
    printError("`TARGET` or `PID` haven't set");
    return;
  }

  if (pid) {
    printf("open process by `pid`: %d\n", pid);

    handle = OpenProcess(PROCESS_VM_OPERATION | PROCESS_QUERY_INFORMATION |
                             PROCESS_VM_READ | PROCESS_VM_WRITE,
                         false, pid);

    if (handle) {
      printf("%s: initializing\n", projectName);
      initialize(handle);

      // write
      // mov dword ptr [addr], esp
      // EB FE jmp here
      // pop [esp -4]
      // ret
      // addr to write esp

      for (unsigned int i = 0; i < calls.size(); i++) {
        unsigned long storeEsp;

        storeEsp = (unsigned long)calls[i]->cal + 8 +3;

        VirtualProtectEx(handle, calls[i]->cal, 13 +3, PAGE_EXECUTE_READWRITE,
                         &oldProtect);
        WriteProcessMemory(
            handle, calls[i]->cal,
            "\x55\x8B\xEC\x89\x25\x03\x03\x03\x03\x5D\xC3\x00\x00\x00\x00\x90", 13 +3, 0);
        WriteProcessMemory(handle, (void *)((unsigned long)calls[i]->cal +3 +2),
                           &storeEsp, 4, 0);
      }

      printf("waiting for process to self exit\n");

      for (; ; Sleep(10)) {
        if (WaitForSingleObject(handle, 0) == WAIT_TIMEOUT) {
          printError("proccess terminated by another process\n");
          break;
        }

        for (unsigned int i = 0; i < calls.size(); i++) {
          ReadProcessMemory(handle, (void *)((unsigned long)calls[i]->cal + 8 +3),
                            &esp, 4, 0);

          if (esp) {
            unsigned long espValue;
            unsigned char valueCode;

            ReadProcessMemory(handle, (void *)esp, &espValue, 4, 0);
            ReadProcessMemory(handle, (void *)(espValue - 5), &valueCode, 1, 0);

            if (valueCode == 0x15) {
              // call dword [addr]
              printf("got call `%s` ( call dword [addr] ) at address %08X\n",
                     calls[i]->name, espValue - 6);
            } else if (valueCode == 0xE8) {
              // call addr
              printf("got call `%s` ( call addr ) at address %08X\n",
                     calls[i]->name, espValue - 5);
            } else {
              ReadProcessMemory(handle, (void *)(espValue - 2), &valueCode, 1,
                                0);

              if (valueCode == 0xFF) {
                // possible call register
                printf("got call `%s` ( call register ) at address %08X\n",
                       calls[i]->name, espValue - 2);
              } else {
                printf("got call `%s` and it would return to %08X\n",
                       calls[i]->name, espValue);
              }
            }

            printf("was at address %08X esp %08X\n", (unsigned long)calls[i]->cal + 6, esp);

            // and null to get new
            esp = 0;

            WriteProcessMemory(handle,
                               (void *)((unsigned long)calls[i]->cal + 8 + 3),
                               &esp, 4, 0);

          } // [if: esp]

          // for loop gonna end if esp is filled
        }
      }

      printf("Done\n");
    } else {
      printError("haven't found priccess\n");
    }
  } else {
    printError("didnt't find process\n");
  }
}

int main(int argc, char *argv[]) {
  loadOptions();

  if (_stricmp(argv[1], "set") == 0) {
    setOption(1, argv);
  } else if (_stricmp(argv[1], "options") == 0) {
    showOptions();
  } else if (_stricmp(argv[1], "run") == 0) {
    run();
  }

  return 0;
}