#include "consts.h"
#include "prints.h"

#include <ctype.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>

#define size_to_scan 786

const char *strings[] = {
  "cheat",         "Cheat",         "Proccess List", "proccess list",
  "Memory Viewer", "memory viewer", "Dissect",       "dissect",
  "Pointer scan",  "pointer scan",  "Add address",   "add address",
  "Process",       "Memory",        "Change address"
};

const char *ignoreDlls[] = {
    "ntdll.dll",      "KERNEL32.DLL",
    "KERNELBASE.dll", "apphelp.dll",
    "IMM32.dll",      "USER32.dll",
    "win32u.dll",     "d3d9.dll",
    "GDI32.dll",      "msvcrt.dll",
    "gdi32full.dll",  "DSOUND.dll",
    "WINMM.dll",      "sechost.dll",
    "WS2_32.dll",     "RPCRT4.dll",
    "advapi32.dll",   "ole32.dll",
    "SspiCli.dll",    "combase.dll",
    "CRYPTBASE.dll",  "powrprof.dll",
    "ucrtbase.dll",   "bcryptPrimitives.dll",
    "OLEAUT32.dll",   "msvcp_win.dll",
    "WINMMBASE.dll",  "VERSION.dll",
    "cfgmgr32.dll",   "SHLWAPI.dll",
    "dwmapi.dll",     "COMDLG32.dll",
    "shcore.dll",     "SHELL32.dll",
    "COMCTL32.dll",   "urlmon.dll",
    "OLEACC.dll",     "windows.storage.dll",
    "WINSPOOL.DRV",   "kernel.appcore.dll",
    "profapi.dll",    "dbghelp.dll",
    "iertutil.dll",   "bcrypt.dll",
    "uxtheme.dll",    "mswsock.dll",
    "d3d10warp.dll",  "D3DREF9.DLL",
    "MSCTF.dll",      "clbcatq.dll",
    "MMDevApi.dll",   "DEVOBJ.dll",
    "PROPSYS.dll",    "wintypes.dll",
    "AUDIOSES.DLL",   "avrt.dll",
    "winsta.dll",     "MSACM32.dll",
    "CRYPTSP.dll",    "rsaenh.dll",
    "WTSAPI32.dll",   "rdpendp.dll",
    "WININET.dll",    "IPHLPAPI.DLL",


};

const char writee[] = "????????????????????????????????????????????????????????"
                      "??????????????????????????????????";

char TARGET[256];
char PID[256];

char *toUpper(char *pArray) {
  int arrayLength = strlen(pArray);

  for (int i = 0; i < arrayLength; i++) {
    if (pArray[i] >= 'a' && pArray[i] <= 'z')
      pArray[i] -= ' ';
  }

  return pArray;
}

bool isNumberArray(char *string) {
  for (int i = 0; i < strlen(string); i++) {
    if (!isdigit(string[i])) {
      return false;
    }
  }

  return true;
}

void loadOptions() {
  std::string line;
  std::ifstream infile("options");

  while (std::getline(infile, line)) {
    char *arg = strtok((char *)line.c_str(), ":");

    if (_stricmp(arg, "TARGET") == 0) {
      memcpy(TARGET, line.c_str() + 7, strlen(line.c_str() + 7) + 1);
    } else if (_stricmp(arg, "PID") == 0) {
      memcpy(PID, line.c_str() + 4, strlen(line.c_str() + 4) + 1);
    }
  }
}

void setOption(int i, char *argv[]) {
  if (_stricmp(argv[i + 1], "TARGET") == 0) {
    PID[0] = 0x0;
    memcpy(TARGET, argv[i + 2], strlen(argv[i + 2]) + 1);
  } else if (_stricmp(argv[i + 1], "PID") == 0) {
    if (isNumberArray(argv[i + 2])) {
      TARGET[0] = 0x0;
      memcpy(PID, argv[i + 2], strlen(argv[i + 2]) + 1);
    }
  }

  std::ofstream file;
  file.open("options");
  file << "TARGET:" << TARGET << "\n";
  file << "PID:" << PID << "\n";
  file.close();

  printf("%s: set `%s` = %s\n", projectName, toUpper(argv[i + 1]), argv[i + 2]);
}

void showOptions() {
  printf("Option                 Value  Description\n");
  printf("------                 -----  -----------\n");
  printf("TARGET  %20s  process name to target only one can be selected\n",
         TARGET);
  printf("PID     %20s  process id to target\n", PID);
}

unsigned long getProc(char *name) {
  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32)) {
    if (_stricmp(pe32.szExeFile, name) == 0) {
      return pe32.th32ProcessID;
    }
  }
  return 0;
}

unsigned long search(void *handle, unsigned long start, unsigned long size,
                     const char *bytesToSearch, unsigned long sizeToSearch,
                     unsigned long step) {
  unsigned char bytes[size_to_scan];
  unsigned long gotCount = 0;

  for (unsigned long i = 0; i < size; i += (size_to_scan - sizeToSearch)) {
    ReadProcessMemory(handle, (PVOID)(start + i), &bytes, size_to_scan, 0);
    for (unsigned long j = 0; j < size_to_scan; j++) {
      bool failed = false;

      for (unsigned char k = 0; k < sizeToSearch; k++) {
        if (*(unsigned char *)((unsigned long)&bytes + j + k) !=
            (unsigned char)bytesToSearch[k]) {
          failed = true;
          break;
        }
      }

      if (failed == false) {
        gotCount++;
        if (gotCount == step)
          return start + i + j;
      }
    }
  }
  return 0;
}

void initialize() {}

void run() {
  void *handle;
  unsigned long pid;
  unsigned long oldProtect;

  printf("%s: initializing\n", projectName);
  initialize();

  if (TARGET[0]) {
    printf("using `TARGET`: %s to find proccess\n", TARGET);
    pid = getProc(TARGET);
  } else if (PID[0]) {
    pid = atoi(PID);
  } else {
    printError("`TARGET` or `PID` haven't set");
    return;
  }

  if (pid) {
    printf("open process by `pid`: %d\n", pid);

    handle = OpenProcess(
        PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, false, pid);

    if (handle) {
      printf("searching %d strings\n", sizeof(strings) / sizeof(strings[0]));

      HMODULE hMods[1024];
      DWORD cbNeeded;
      unsigned int k;
      unsigned long foundStrings = 0;

      if (EnumProcessModules(handle, hMods, sizeof(hMods), &cbNeeded)) {
        for (k = 0; k < (cbNeeded / sizeof(HMODULE)); k++) {
          TCHAR szModName[MAX_PATH];

          if (GetModuleFileNameEx(handle, hMods[k], szModName,
                                  sizeof(szModName) / sizeof(TCHAR))) {
            unsigned int j;
            for (j = strlen(szModName) - 1; j > 0; j--) {
              if (szModName[j] == '\\') {
                j++;
                break;
              }
            }

            unsigned int l;
            for (l = 0; l < sizeof(ignoreDlls) / sizeof(ignoreDlls[0]); l++) {
              if (_stricmp(ignoreDlls[l], szModName + j) == 0) {
                l = 0;
                break;
              }
            }

            if (l != 0) {
              printf("start searching at: %s, found strings: %d\n",
                     szModName + j, foundStrings);

              unsigned int i;
              for (i = 0; i < sizeof(strings) / sizeof(strings[0]); i++) {
                unsigned long addr = (unsigned long)hMods[k];

                for (; addr != 0;) {
                  addr = search(handle, addr, 0x01F00000, strings[i],
                                strlen(strings[i]), 1);

                  if (addr != 0) {
                    printf("found `%s` at %08X from %s\n", strings[i], addr,
                           szModName + j);

                    VirtualProtectEx(handle, (void *)addr, strlen(strings[i]),
                                     PAGE_EXECUTE_READWRITE, &oldProtect);
                    WriteProcessMemory(handle, (void *)addr, writee,
                                       strlen(strings[i]), 0);

                    foundStrings++;
                    // not to get same string as we searched
                    addr += strlen(strings[i]);
                  }
                }
              } // [ loop: i ]
            } // [ if: l != 0 ]
          }
        }
      }

      printf("Done\n");
    } else {
      printError("haven't found priccess\n");
    }
  } else {
    printError("didnt't find process\n");
  }
}

int main(int argc, char *argv[]) {
  loadOptions();

  if (_stricmp(argv[1], "set") == 0) {
    setOption(1, argv);
  } else if (_stricmp(argv[1], "options") == 0) {
    showOptions();
  } else if (_stricmp(argv[1], "run") == 0) {
    run();
  }

  return 0;
}