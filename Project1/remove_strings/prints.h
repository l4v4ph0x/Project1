#ifndef PRINTS
#define PRINTS

#include <stdarg.h>
#include <stdio.h>
#include <vector>

#include "consts.h"

void printError(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  printf("\n%s: [ERROR]: ", projectName);
  vfprintf(stdout, fmt, args);
  printf("\n\n");

  va_end(args);
}

#endif
