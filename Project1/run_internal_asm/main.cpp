#include "consts.h"
#include "prints.h"

#include <ctype.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <windows.h>
#include <tlhelp32.h>

char fileBase[] =
    "#include <stdio.h>\n"
    "#include <windows.h>\n"
    "_declspec(naked) void asmfunc() {\n"
    "__asm {\n"
    ":asm\n"
    "}\n"
    "}\n"
    "_declspec(naked) void endfunc() {}\n"
    "int main() {\n"
    "void *target = OpenProcess(\n"
    "PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ |\n"
    "PROCESS_VM_WRITE | PROCESS_VM_OPERATION,\n"
    "FALSE, :pid);\n"
    "void *alloc = \n"
    "VirtualAllocEx(target, NULL, \n"
    "(unsigned long)&endfunc - (unsigned long)&asmfunc,\n"
    "MEM_RESERVE | MEM_COMMIT,\n"
    "PAGE_EXECUTE_READWRITE);\n"
    "WriteProcessMemory(target, alloc, &asmfunc,\n"
    "(unsigned long)&endfunc - (unsigned long)&asmfunc,\n"
    "NULL);\n"
    "void *thread = CreateRemoteThread(\n"
    "target, NULL, 0, (LPTHREAD_START_ROUTINE)alloc, alloc, NULL, NULL);\n"
    "if (thread) {\n"
    "printf(\"success %08X\\n\", (unsigned int)alloc);\n"
    "} else {\n"
    "printf(\"failed\\n\");\n"
    "}"
    "}\n"

    ;

char TARGET[256];
char PID[256];
char ASMFILE[256];
char *ASM;

char *toUpper(char *pArray) {
  int arrayLength = strlen(pArray);

  for (int i = 0; i < arrayLength; i++) {
    if (pArray[i] >= 'a' && pArray[i] <= 'z')
      pArray[i] -= ' ';
  }

  return pArray;
}

bool isNumberArray(char *string) {
  for (int i = 0; i < strlen(string); i++) {
    if (!isdigit(string[i])) {
      return false;
    }
  }

  return true;
}

std::string readFile(char *filename) {
  std::ifstream ifs(filename);
  std::string content((std::istreambuf_iterator<char>(ifs)),
                      (std::istreambuf_iterator<char>()));

  return content;
}

void loadOptions() {
  std::string line;
  std::ifstream infile("options");

  bool readingASM = false;
  std::string asmLines = "";

  while (std::getline(infile, line)) {
    char *arg = strtok((char *)line.c_str(), ":");
    if (arg) {
      if (_stricmp(arg, "TARGET") == 0) {
        memcpy(TARGET, line.c_str() + 7, strlen(line.c_str() + 7) + 1);
      } else if (_stricmp(arg, "PID") == 0) {
        memcpy(PID, line.c_str() + 4, strlen(line.c_str() + 4) + 1);
      } else if (_stricmp(arg, "ASMFILE") == 0) {
        memcpy(ASMFILE, line.c_str() + 8, strlen(line.c_str() + 8) + 1);
      } else if (_stricmp(arg, "ASM") == 0) {
        readingASM = true;
      } else if (readingASM) {
        asmLines.append(line + "\n");
      }
    }
  }

  ASM = (char *)malloc(asmLines.length() + 1);
  memcpy(ASM, asmLines.c_str(), asmLines.length() + 1);
}

void setOption(int i, char *argv[]) {
  if (_stricmp(argv[i + 1], "TARGET") == 0) {
    PID[0] = 0x0;
    memcpy(TARGET, argv[i + 2], strlen(argv[i + 2]) + 1);
  } else if (_stricmp(argv[i + 1], "PID") == 0) {
    if (isNumberArray(argv[i + 2])) {
      TARGET[0] = 0x0;
      memcpy(PID, argv[i + 2], strlen(argv[i + 2]) + 1);
    }
  } else if (_stricmp(argv[i + 1], "ASMFILE") == 0) {
    ASM[0] = 0x0;
    memcpy(ASMFILE, argv[i + 2], strlen(argv[i + 2]) + 1);
  } else if (_stricmp(argv[i + 1], "ASM") == 0) {
    char buf[256];
    unsigned int lineNum;
    std::string asmLines = "";

    printf("start writting assembly code (';' to finish):\n");

    for (lineNum = 1; strcmp(buf, ";\n") != 0; lineNum++) {
      printf("%03d > ", lineNum);
      fgets(buf, sizeof(buf), stdin);

      if (strcmp(buf, ";\n") != 0) {
        asmLines.append(std::string(buf));
      }
    }

    ASMFILE[0] = 0x0;
    delete[] ASM;
    ASM = (char *)malloc(asmLines.length() + 1);
    memcpy(ASM, asmLines.c_str(), asmLines.length() + 1);
  }

  std::ofstream file;
  file.open("options");
  file << "TARGET:" << TARGET << "\n";
  file << "PID:" << PID << "\n";
  file << "ASMFILE:" << ASMFILE << "\n";
  file << "ASM:\n" << ASM << "\n";
  file.close();

  // asm lines have eception of printing result
  if (_stricmp(argv[i + 1], "ASM") == 0) {
    printf("\n%s: set `%s` = \n%s\n", projectName, toUpper(argv[i + 1]), ASM);
  } else {
    printf("%s: set `%s` = %s\n", projectName, toUpper(argv[i + 1]),
           argv[i + 2]);
  }
}

void showOptions() {
  printf("Option                 Value  Description\n");
  printf("------                 -----  -----------\n");
  printf("TARGET   %20s  process name to target only one can be selected\n",
         TARGET);
  printf("PID      %20s  process id to target\n", PID);
  printf("ASMFILE  %20s  assembly file to execute in process\n", ASMFILE);
  printf("ASM:                           assembly to run in process\n%s\n", ASM);
}

unsigned long getProc(char *name) {
  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32)) {
    if (_stricmp(pe32.szExeFile, name) == 0) {
      return pe32.th32ProcessID;
    }
  }
  return 0;
}

void find_and_replace(std::string &source, std::string const &find,
                      std::string const &replace) {
  for (std::string::size_type i = 0;
       (i = source.find(find, i)) != std::string::npos;) {
    source.replace(i, find.length(), replace);
    i += replace.length();
  }
}

void initialize() {}

void run() {
  void *handle;
  unsigned long pid;
  unsigned long oldProtect;

  printf("%s: initializing\n", projectName);
  initialize();

  if (TARGET[0]) {
    printf("using `TARGET`: %s to find proccess\n", TARGET);
    pid = getProc(TARGET);
  } else if (PID[0]) {
    pid = atoi(PID);
  } else {
    printError("`TARGET` or `PID` haven't set");
    return;
  }

  if (pid) {
    printf("open process by `pid`: %d\n", pid);

    handle =
        OpenProcess(PROCESS_VM_OPERATION | PROCESS_SUSPEND_RESUME, false, pid);

    if (handle) {
      // write cpp file

      char buf[2048];
      std::ofstream file;
      std::string fileStr = std::string(fileBase);

      sprintf(buf, "%d", pid);
      find_and_replace(fileStr, ":pid", buf);
      find_and_replace(fileStr, ":asm",
                       strcmp(ASMFILE, "") == 0 ? ASM : readFile(ASMFILE));

      file.open("exec.cpp");
      file << fileStr;
      file.close();

      sprintf(buf,
              "\"%s\" /EHsc /I include exec.cpp lib\\LIBCMT.lib "
              "lib\\oldnames.lib lib\\onecore.lib lib\\libvcruntime.lib "
              "lib\\libucrt.lib lib\\uuid.lib lib\\user32.lib",
              "cl.exe");
      system(buf);
      printf("\nRun It!\n\n");
      system("exec.exe");

      printf("\nDone\n");
    } else {
      printError("haven't found priccess\n");
    }
  } else {
    printError("didnt't find process\n");
  }
}

int main(int argc, char *argv[]) {
  loadOptions();

  if (_stricmp(argv[1], "set") == 0) {
    setOption(1, argv);
  } else if (_stricmp(argv[1], "options") == 0) {
    showOptions();
  } else if (_stricmp(argv[1], "run") == 0) {
    run();
  }

  return 0;
}