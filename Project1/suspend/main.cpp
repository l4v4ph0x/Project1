#include "consts.h"
#include "prints.h"

#include <ctype.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <windows.h>
#include <tlhelp32.h>

typedef LONG(NTAPI *pZwSuspendProcess)(IN HANDLE ProcessHandle);
typedef LONG(NTAPI *pZwResumeProcess)(IN HANDLE ProcessHandle);

pZwSuspendProcess ZwSuspendProcess;
pZwResumeProcess ZwResumeProcess;

char TARGET[256];
char PID[256];
char STATE[256];

char *toUpper(char *pArray) {
  int arrayLength = strlen(pArray);

  for (int i = 0; i < arrayLength; i++) {
    if (pArray[i] >= 'a' && pArray[i] <= 'z')
      pArray[i] -= ' ';
  }

  return pArray;
}

bool isNumberArray(char *string) {
  for (int i = 0; i < strlen(string); i++) {
    if (!isdigit(string[i])) {
      return false;
    }
  }

  return true;
}

void loadOptions() {
  std::string line;
  std::ifstream infile("options");

  while (std::getline(infile, line)) {
    char *arg = strtok((char *)line.c_str(), ":");

    if (_stricmp(arg, "TARGET") == 0) {
      memcpy(TARGET, line.c_str() + 7, strlen(line.c_str() + 7) + 1);
    } else if (_stricmp(arg, "PID") == 0) {
      memcpy(PID, line.c_str() + 4, strlen(line.c_str() + 4) + 1);
    } else if (_stricmp(arg, "STATE") == 0) {
      memcpy(STATE, line.c_str() + 6, strlen(line.c_str() + 6) + 1);
    }
  }
}

void setOption(int i, char *argv[]) {
  if (_stricmp(argv[i + 1], "TARGET") == 0) {
    PID[0] = 0x0;
    memcpy(TARGET, argv[i + 2], strlen(argv[i + 2]) + 1);
  } else if (_stricmp(argv[i + 1], "PID") == 0) {
    if (isNumberArray(argv[i + 2])) {
      TARGET[0] = 0x0;
      memcpy(PID, argv[i + 2], strlen(argv[i + 2]) + 1);
    }
  } else if (_stricmp(argv[i + 1], "STATE") == 0) {
    if (_stricmp(argv[i + 2], "suspend") == 0 ||
        _stricmp(argv[i + 2], "resume") == 0) {
      memcpy(STATE, argv[i + 2], strlen(argv[i + 2]) + 1);
    }
  }

  std::ofstream file;
  file.open("options");
  file << "TARGET:" << TARGET << "\n";
  file << "PID:" << PID << "\n";
  file << "STATE:" << STATE << "\n";
  file.close();

  printf("%s: set `%s` = %s\n", projectName, toUpper(argv[i + 1]), argv[i + 2]);
}

void showOptions() {
  printf("Option                 Value  Description\n");
  printf("------                 -----  -----------\n");
  printf("TARGET  %20s  process name to target only one can be selected\n",
         TARGET);
  printf("PID     %20s  process id to target\n", PID);
  printf("STATE   %20s  suspend/resume\n", STATE);
}

unsigned long getProc(char *name) {
  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32)) {
    if (_stricmp(pe32.szExeFile, name) == 0) {
      return pe32.th32ProcessID;
    }
  }
  return 0;
}

void initialize() {
  HMODULE ntdll = GetModuleHandleA("ntdll.dll");

  ZwSuspendProcess =
      (pZwSuspendProcess)GetProcAddress(ntdll, "ZwSuspendProcess");
  ZwResumeProcess = (pZwResumeProcess)GetProcAddress(ntdll, "ZwResumeProcess");
}

void run() {
  void *handle;
  unsigned long pid;

  printf("%s: initializing\n", projectName);
  initialize();

  if (TARGET[0]) {
    printf("using `TARGET`: %s to find proccess\n", TARGET);
    pid = getProc(TARGET);
  } else if (PID[0]) {
    pid = atoi(PID);
  } else {
    printError("`TARGET` or `PID` haven't set");
    return;
  }

  if (pid) {
    printf("open process by `pid`: %d\n", pid);

    handle = OpenProcess(PROCESS_VM_OPERATION | PROCESS_SUSPEND_RESUME, false, pid);

    if (handle) {
      if (_stricmp(STATE, "suspend") == 0) {
        ZwSuspendProcess(handle);
      } else if (_stricmp(STATE, "resume") == 0) {
        ZwResumeProcess(handle);
      } else {
        printError("`STATE` haven't set to suspend or resume");
        return;
      }

      printf("Done\n");
    } else {
      printError("haven't found priccess\n");
    }
  } else {
    printError("didnt't find process\n");
  }
}

int main(int argc, char *argv[]) {
  loadOptions();

  if (_stricmp(argv[1], "set") == 0) {
    setOption(1, argv);
  } else if (_stricmp(argv[1], "options") == 0) {
    showOptions();
  } else if (_stricmp(argv[1], "run") == 0) {
    run();
  }

  return 0;
}